import os

def rename_files():
	#Get the Files in the directory
	file_list = os.listdir("/home/abubakker/udacity/Full Stack Development I/Basic Python code/2_secret_message")
	print(file_list)
	saved_path = os.getcwd()
	print("Current working Directroy is"+saved_path)
	os.chdir("/home/abubakker/udacity/Full Stack Development I/Basic Python code/2_secret_message")
	#Renaming each file namerent
	for file_name in file_list:
		# translate definatiion : https://www.tutorialspoint.com/python/string_translate.html
		# in python 2.7 : os.rename(file_name,file_name.translate(None,"0123456789"))
		print ("Old file name", file_name)
		print ("Renamed file name",file_name.translate(str.maketrans('','','1234567890')))
		os.rename(file_name,file_name.translate(str.maketrans('','','1234567890')))
	os.chdir(saved_path)

rename_files()
