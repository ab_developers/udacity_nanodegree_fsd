import turtle
def draw_objects_on_screen() :

    #initializing screen
    window = turtle.Screen()

    #Setting bakground colour
    window.bgcolor("red")

    #Drawing objects
    draw_square(100,"blue","classic",2,90)
    draw_circle(100,"black","turtle",2)
    draw_triangle(120,"green","arrow",2,120)

    #exiting on click
    window.exitonclick()

def draw_square(distance,color,shape,speed,degree):
    #initializing turtle
    square_object = turtle.Turtle()

    #defining shape, color and speed
    square_object.shape(shape)
    square_object.color(color)
    square_object.speed(speed)

    #square will be having 4 sides so count will be 4
    loop_count = 4
    line_drawn = 0
    while(line_drawn < loop_count) :

        #drawing a side with distance and rotating with degree
        square_object.forward(distance)
        square_object.right(degree)
        line_drawn = line_drawn + 1
    return

def draw_triangle(distance,color,shape,speed,degree):
    #initializing turtle
    triangle_object = turtle.Turtle()

    #defining shape, color and speed
    triangle_object.shape(shape)
    triangle_object.color(color)
    triangle_object.speed(speed)

    triangle_object.left(240)

    #triangle will be having 3 sides so count will be 3
    loop_count = 3
    line_drawn = 0
    while(line_drawn < loop_count) :
        triangle_object.forward(distance)

        #no need to rotate last count as only 2 sides need rotation
        if(line_drawn < (loop_count - 1)) :
            triangle_object.right(degree)
        line_drawn = line_drawn + 1
    return

def draw_circle(radius,color,shape,speed):

    #initializing turtle
    circle_object = turtle.Turtle()

    #defining shape, color and speed
    circle_object.shape(shape)
    circle_object.color(color)
    circle_object.speed(speed)

    #drawing circle
    circle_object.circle(radius)
    return

draw_objects_on_screen()
