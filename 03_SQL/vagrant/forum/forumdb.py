# "Database code" for the DB Forum.

import datetime
import psycopg2

DBNAME = "forum"

#POSTS = [("This is the first post.", datetime.datetime.now())]

def get_posts():
  """Return all posts from the 'database', most recent first."""
  database_connection = psycopg2.connect(database=DBNAME)
  database_cursor = database_connection.cursor()
  database_cursor.execute("select content, time from posts order by time DESC")
  posts = database_cursor.fetchall()
  database_connection.close()
  return posts


def add_post(content):
  """Add a post to the 'database' with the current timestamp."""
  database_connection = psycopg2.connect(database=DBNAME)
  database_cursor = database_connection.cursor()
  database_cursor.execute("insert into posts values(%s)",(content,))
  database_connection.commit()
  database_connection.close()


