Steps to deployment

Here's an overview of the steps you'll need to complete. We'll be going over each one in more detail.

    -Check your server code into a new local Git repository.
    -Sign up for a free Heroku account.
    -Download the Heroku command-line interface (CLI).
    -Authenticate the Heroku CLI with your account: heroku login
    -Create configuration files Procfile, requirements.txt, and runtime.txt and check them into your Git repository.
    -Modify your server to listen on a configurable port.
    -Create your Heroku app: heroku create your-app-name
    -Push your code to Heroku with Git: git push heroku master


Check in your code

Heroku (and many other web hosting services) works closely with Git: you can deploy a particular version of your code to Heroku by pushing it with the git push command. So in order to deploy your code, it first needs to be checked into a local Git repository.

This Git repository should be separate from the one created when you downloaded the exercise code (the course-ud303 directory). Create a new directory outside of that directory and copy the bookmark server code (the file BookmarkServer.py from last lesson) into it. Then set this new directory up as a Git repository:

    git init
    git add BookmarkServer.py
    git commit -m "Checking in my bookmark server!"




Sign up for a free Heroku account
First, visit this link and follow the instructions to sign up for a free Heroku account:

https://signup.heroku.com/dc


Install the Heroku CLI and authenticate

You'll need the Heroku command-line interface (CLI) tool to set up and configure your app. Download and install it now. Once you have it installed, the heroku command will be available in your shell.



Read more : https://classroom.udacity.com/nanodegrees/nd004-intr2/parts/7ce95b94-ecff-4bb2-bf23-07df34a760b8/modules/b08bb93f-d13e-42d6-a60c-8c7d4d263c42/lessons/773150bb-8e88-4457-b077-3b8a02018f33/concepts/1d84f620-4d25-45fd-aa10-276498e328ae
